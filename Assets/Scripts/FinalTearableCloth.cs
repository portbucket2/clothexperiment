﻿using TMPro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public class FinalTearableCloth : MonoBehaviour
{
    public static FinalTearableCloth instance;
    public Transform pinPoinstHolder;
    public TMP_InputField xInputField, yInputField, gravityField, tearSensitivityField, cuttingSizeField, accuracyInput;



    // Where we'll store all of the points
    public List<PointMass> pointmasses;
    //Physics physics;
    
    //[HideInInspector]
    public float gravity = -50;
    //[HideInInspector]
    public float clothTearSensitivity;
    //[HideInInspector]
    public Vector3[] vertices;
    //[HideInInspector]
    public int[] triangles;

    MeshFilter meshFilter;
    MeshRenderer meshRenderer;
    Mesh mesh;

    public int ySize = 15;
    public int xSize = 25;

    private void Awake() {

        instance = this;
        pinPoinstHolder.gameObject.SetActive(false);

        GameManager.instance.fabricView.isOn = false;
        xInputField.text = "75";
        yInputField.text = "75";
        gravityField.text = "5";
        tearSensitivityField.text = "200";
        accuracyInput.text = "3";
    }

    private void GetSizeInput() {

        xSize = int.Parse(xInputField.text);
        ySize = int.Parse(yInputField.text);
        gravity = int.Parse(gravityField.text) * -1;
        clothTearSensitivity = int.Parse(tearSensitivityField.text);
        GameManager.instance.accuracy = int.Parse(accuracyInput.text);
        //GameManager.instance.mouseTearSize = float.Parse(cuttingSizeField.text);
    }

    public IEnumerator Setup() {

        GetSizeInput();
        Camera.main.transform.position = new Vector3(xSize / 2, -ySize / 2, -xSize * 2);
        pinPoinstHolder.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, -0.1f);
        pinPoinstHolder.localScale = new Vector3(ySize / 10, ySize / 10, 1);
        GameManager.instance.allLinesToRender.Clear();
        yield return new WaitForSeconds(0.15f);
        
        // We use an List instead of an array so we can add or remove PointMasss at will.
        pointmasses = new List<PointMass>();

        // create the curtain
        CreateTearableCloth();
        CreateClothMesh();

        pinPoinstHolder.gameObject.SetActive(true);
    }

    private void CreateTearableCloth() {

        vertices = new Vector3[(xSize + 1) * (ySize + 1)];
        triangles = new int[xSize * ySize * 6];
        int vertIndex = 0, trisIndex = 0;

        for (int y = 0; y <= ySize; y++) {
            for (int x = 0; x <= xSize; x++) {

                Vector3 pos = new Vector3( x , -y, 0);
                globalZ = 0f;
                bool hasTriangle = false;
                if(x > 0 && y > 0) {

                    // Calculate Triangles
                    hasTriangle = true;
                    triangles[trisIndex++] = ((y - 1) * (xSize + 1)) + x - 1;
                    triangles[trisIndex++] = ((y - 1) * (xSize + 1)) + x;
                    triangles[trisIndex++] = (y * (xSize + 1)) + x - 1;
                    triangles[trisIndex++] = (y * (xSize + 1)) + x;
                    triangles[trisIndex++] = (y * (xSize + 1)) + x - 1;
                    triangles[trisIndex++] = ((y - 1) * (xSize + 1)) + x;
                }


                PointMass pointmass = new PointMass(pos, trisIndex - 6, hasTriangle);
                vertices[vertIndex++] = new Vector3(pos.x, pos.y, globalZ);

                if (x != 0)
                    pointmass.attachTo(pointmasses[pointmasses.Count - 1], GameManager.instance.restingDistances, GameManager.instance.clothHardness, GameManager.instance.tearSensitivity);
                  
                if (y != 0)
                    pointmass.attachTo(pointmasses[((y - 1) * (xSize + 1)) + x], GameManager.instance.restingDistances, GameManager.instance.clothHardness, GameManager.instance.tearSensitivity);

                // we pin the very top PointMasss to where they are
                if (y == 0)
                    pointmass.PinHook(pointmass.x, pointmass.y, pointmass.z);

                // add to PointMass to list
                pointmasses.Add(pointmass);
            }
        }
        CreatePins();
    }

    float globalZ;
    private void Update() {

        if (pointmasses == null)
            return;

        for (int i = 0; i < pointmasses.Count; i++)
        {

            pointmasses[i].UpdatePhysics(Time.deltaTime);
            vertices[i] = new Vector3(pointmasses[i].x, pointmasses[i].y, pointmasses[i].z);
            pointmasses[i].UpdateInteractions();
        }
        mesh.vertices = vertices;
        mesh.triangles = triangles;
    }
    public GameObject prefab;
    public Transform insieCuttingDetector, outSidePointsHolder;
    public Material diceMaterial;
    void CreatePins() {

        string CurrentLevel = "CL";
        if (PlayerPrefs.HasKey(CurrentLevel) != null)
        {

            if (PlayerPrefs.GetInt(CurrentLevel) + 1 >= GameManager.instance.diceCollection.sprites.Length)
                PlayerPrefs.SetInt(CurrentLevel, 0);
            else
                PlayerPrefs.SetInt(CurrentLevel, PlayerPrefs.GetInt(CurrentLevel) + 1);
        }

        pinPoinstHolder.GetComponent<SpriteRenderer>().sprite = GameManager.instance.diceCollection.sprites[PlayerPrefs.GetInt(CurrentLevel, 0)];
        foreach (Transform item in outSidePointsHolder)
        {
            Destroy(item.gameObject);
        }

        PolygonCollider2D points = pinPoinstHolder.gameObject.AddComponent<PolygonCollider2D>();
        for (int i = 0; i < points.points.Length; i++)
        {
            insieCuttingDetector.localPosition = new Vector3(points.points[i].x, points.points[i].y, 0);
            GameObject go = Instantiate(prefab, insieCuttingDetector.position, Quaternion.identity, outSidePointsHolder);
            go.transform.localScale = pinPoinstHolder.localScale / 1.5f;
            int id = Mathf.Abs((int)(Mathf.Round(go.transform.position.y)) * (xSize + 1)) + (int)Mathf.Round(go.transform.position.x);
            pointmasses[id].PinTo();
        }
        GameManager.instance.totalRemainingPointsToCut = points.points.Length;
        insieCuttingDetector.localPosition = Vector3.zero;
    }

    private void CreateClothMesh() {

        mesh = new Mesh {
            name = gameObject.name
        };
        meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.material.SetTextureScale("_MainTex", new Vector2(xSize, ySize));
        meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        mesh.vertices = vertices;
        mesh.triangles = triangles;
    }

    private Vector2[] GetUVs() {

        Vector2[] _uvs = new Vector2[(xSize + 1) * (ySize + 1)];
        int i = 0;
        for (int y = 0; y <= ySize; y += 1) {

            for (int x = 0; x <= xSize; x += 1) {

                _uvs[i] = new Vector2((float)x / xSize, (float)y / ySize);
                i++;
            }
        }
        return _uvs;
    }

    public void UpdateTriangles(int triangleCuttingIndex) {

        for (int i = 0; i < 6; i++) {

           triangles[triangleCuttingIndex++] = 0;
        }
    }

}
